package report

import (
	"goAdapter/report/mqttAliyun"
	mqttEmqx "goAdapter/report/mqttEMQX"
	"goAdapter/report/mqttHuawei"
	"goAdapter/report/mqttThingsBoard"
)

type ReportServiceAPI interface {
	GWLogIn()
	GWLogOut()
	NodesLogIn()
	NodesLogOut()
	GWPropertyReport()
	NodesPropertyReport()
}

func init() {

}

func ReportServiceInit() {

	mqttAliyun.ReportServiceAliyunInit()

	mqttEmqx.ReportServiceEmqxInit()

	mqttHuawei.ReportServiceHuaweiInit()

	mqttThingsBoard.ReportServiceThingsBoardInit()

}
