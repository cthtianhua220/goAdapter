package commInterface

import (
	"encoding/json"
	"goAdapter/setting"
	"goAdapter/utils"
	"net"
	"time"
)

type TcpClientInterfaceParam struct {
	IP       string `json:"IP"`
	Port     string `json:"Port"`
	Timeout  string `json:"Timeout"`  //通信超时
	Interval string `json:"Interval"` //通信间隔
}

type CommunicationTcpClientTemplate struct {
	Name  string                  `json:"Name"`  //接口名称
	Type  string                  `json:"Type"`  //接口类型,比如serial,TcpClient,udp,http
	Param TcpClientInterfaceParam `json:"Param"` //接口参数
	Conn  net.Conn                `json:"-"`     //通信句柄
}

var CommunicationTcpClientMap = make([]*CommunicationTcpClientTemplate, 0)

func (c *CommunicationTcpClientTemplate) Open() bool {
	conn, err := net.DialTimeout("tcp", c.Param.IP+":"+c.Param.Port, 500*time.Millisecond)
	if err != nil {
		setting.ZAPS.Errorf("通信TCP客户端[%s]接口打开失败 %v", c.Name, err)
		c.Conn = nil
		return false
	}
	setting.ZAPS.Debugf("通信TCP客户端[%s]接口打开成功", c.Name)

	c.Conn = conn
	return true
}

func (c *CommunicationTcpClientTemplate) Close() bool {
	if c.Conn != nil {
		err := c.Conn.Close()
		if err != nil {
			setting.ZAPS.Errorf("通信TCP客户端[%s]接口关闭失败 %v", c.Name, err)
			return false
		}
		setting.ZAPS.Debugf("通信TCP客户端[%s]接口关闭成功", c.Name)
	}
	return true
}

func (c *CommunicationTcpClientTemplate) WriteData(data []byte) int {

	if c.Conn != nil {
		cnt, err := c.Conn.Write(data)
		if err != nil {
			setting.ZAPS.Errorf("通信TCP客户端[%s]接口写失败 %v", c.Name, err)
			c.Close()
			c.Open()
			return 0
		}
		return cnt
	} else {
		c.Open()
	}
	return 0
}

func (c *CommunicationTcpClientTemplate) ReadData(data []byte) int {

	if c.Conn != nil {
		cnt, err := c.Conn.Read(data)
		//setting.ZAPS.Debugf("%s,TcpClient read data cnt %v", c.Name, cnt)
		if err != nil {
			//setting.ZAPS.Errorf("%s,TcpClient read err,%v", c.Name, err)
			return 0
		}
		return cnt
	}
	return 0
}

func (c *CommunicationTcpClientTemplate) GetName() string {
	return c.Name
}

func (c *CommunicationTcpClientTemplate) GetTimeOut() string {
	return c.Param.Timeout
}

func (c *CommunicationTcpClientTemplate) GetInterval() string {
	return c.Param.Interval
}

func (c *CommunicationTcpClientTemplate) GetType() int {
	return CommTypeTcpClient
}

func ReadCommTcpClientInterfaceListFromJson() bool {

	data, err := utils.FileRead("/selfpara/commTcpClientInterface.json")
	if err != nil {
		setting.ZAPS.Debugf("打开通信接口[Tcp客户端]通信接口配置json文件失败 %v", err)
		return false
	}
	err = json.Unmarshal(data, &CommunicationTcpClientMap)
	if err != nil {
		setting.ZAPS.Errorf("通信接口[Tcp客户端]通信接口配置json文件格式化失败 %v", err)
		return false
	}
	setting.ZAPS.Debugf("打开通信接口[Tcp客户端]通信接口配置json文件成功")
	return true
}

func WriteCommTcpClientInterfaceListToJson() {

	utils.DirIsExist("./selfpara")

	sJson, _ := json.Marshal(CommunicationTcpClientMap)
	err := utils.FileWrite("/selfpara/commTcpClientInterface.json", sJson)
	if err != nil {
		setting.ZAPS.Errorf("写入通信接口[Tcp客户端]通信接口配置json文件 %s %v", "失败", err)
		return
	}
	setting.ZAPS.Infof("写入通信接口[Tcp客户端]通信接口配置json文件 %s", "成功")
}
