package mqttThingsBoard

import (
	"encoding/json"
	"goAdapter/setting"
)

type MQTTNodeLogoutTemplate struct {
	Device string `json:"device"`
}

func MQTTThingsBoardNodeLogOut(param ReportServiceGWParamThingsBoardTemplate, nodeMap []string) int {

	nodeLogout := MQTTNodeLogoutTemplate{}

	for _, v := range nodeMap {
		nodeLogout.Device = v

		sJson, _ := json.Marshal(nodeLogout)
		logoutTopic := "v1/gateway/disconnect"

		setting.ZAPS.Infof("上报服务[%s]发布节点离线线消息主题%s", param.ServiceName, logoutTopic)
		setting.ZAPS.Debugf("上报服务[%s]发布节点离线消息内容%s", param.ServiceName, sJson)

		if param.MQTTClient != nil {
			token := param.MQTTClient.Publish(logoutTopic, 0, false, sJson)
			token.Wait()
		}
	}
	return 0
}

func (r *ReportServiceParamThingsBoardTemplate) NodeLogOut(name []string) bool {

	nodeMap := make([]string, 0)
	status := false

	setting.ZAPS.Debugf("上报服务[%s]节点%s离线", r.GWParam.ServiceName, name)
	for _, d := range name {
		for _, v := range r.NodeList {
			if d == v.Name {
				nodeMap = append(nodeMap, v.Param.ClientID)
				MQTTThingsBoardNodeLogOut(r.GWParam, nodeMap)
			}
		}
	}

	return status
}
